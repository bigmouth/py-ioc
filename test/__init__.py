# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/1.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from app.application_context import ApplicationContext

ioc = ApplicationContext()
