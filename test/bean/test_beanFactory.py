# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/8/31.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from unittest import TestCase

from app.bean.factory.normal_bean_factory import NormalBeanFactory


class TestBean:

    def hello(self):
        return "hello"


class TestBaseBean:
    pass


class TestChildBean(TestBaseBean):
    pass


class TestBeanFactory(TestCase):

    def test_get_bean(self):
        factory = NormalBeanFactory()
        factory.add_bean_by_name("aaa", TestBean)
        factory.add_bean_by_type(TestBean)
        factory.add_bean_by_type(TestChildBean)
        factory.add_bean_by_interface(TestBaseBean, TestChildBean)

        b1 = factory.get_bean("aaa")
        self.assertEqual(b1.__class__.__name__, TestBean.__name__)

        b2 = factory.get_bean(TestBean)
        self.assertEqual(b2.__class__.__name__, TestBean.__name__)

        b3 = factory.get_bean(TestBaseBean)
        self.assertEqual(b3.__class__.__name__, TestChildBean.__name__)
