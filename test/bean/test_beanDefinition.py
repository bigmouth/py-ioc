# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/8/31.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from unittest import TestCase

from app.bean.bean_definition import BeanDefinition


class TestBaseBean:
    pass


class TestBean(TestBaseBean):
    pass


class TestBeanDefinition(TestCase):
    def test_get_class_name(self):
        bean = BeanDefinition(TestBean)
        bean_name = bean.get_class_name()
        self.assertEqual("test_beanDefinition.TestBean", bean_name)

    def test_get_interface_name(self):
        bean = BeanDefinition(TestBean)
        bean_name = bean.get_interface_name()
        self.assertEqual("test_beanDefinition.TestBaseBean", bean_name)
        bean = BeanDefinition(TestBaseBean)
        bean_name = bean.get_interface_name()
        self.assertIsNone(bean_name)
