#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-02.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from test import ioc
from test.demo.domain.repository.role_repo import RoleRepo


@ioc.reg_component()
class RoleService:

    @ioc.get_component(RoleRepo)
    def _get_role_repo(self) -> RoleRepo:
        pass

    def get_role_name(self, role_id):
        repo = self._get_role_repo()
        return repo.get_role_name(role_id)