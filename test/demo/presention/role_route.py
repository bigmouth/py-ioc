#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-02.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from test import ioc
from test.demo.application.role_service import RoleService


class RoleRoute:

    @ioc.get_component(RoleService)
    def _get_role_service(self) -> RoleService:
        pass

    def get_role_name(self, role_id):
        service = self._get_role_service()
        return service.get_role_name(role_id)
