#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-02.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from test import ioc
from test.demo.domain.repository.role_repo import RoleRepo


@ioc.reg_component(RoleRepo)
class RoleRepoImpl(RoleRepo):
    def get_role_name(self, role_id):
        return "康熙"
