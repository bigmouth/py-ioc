#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-03.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from app.aop.processor import BaseProcessor, ProcessorContext
from test import ioc


@ioc.reg_aop_processor(".*Service.*")
class TransactionProcessor(BaseProcessor):

    def invoke(self, ctx: ProcessorContext, *args, **kwargs):
        print("开启事务")
        result = self.get_next().invoke(ctx, *args, **kwargs)
        print("结束事务")
        return result
