# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/2.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from app.aop.processor import BaseProcessor, ProcessorContext
from test import ioc


@ioc.reg_aop_processor(".*Service.*")
class LogProcessor(BaseProcessor):

    def invoke(self, ctx: ProcessorContext, *args, **kwargs):
        print(f"打印日志:{ctx.class_name}.{ctx.method_name}, args:{args}, kwargs:{kwargs}")
        return self.get_next().invoke(ctx, *args, **kwargs)
