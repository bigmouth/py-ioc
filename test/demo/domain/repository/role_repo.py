#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-02.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from abc import abstractmethod, ABCMeta


class RoleRepo(metaclass=ABCMeta):

    @abstractmethod
    def get_role_name(self, role_id):
        pass
