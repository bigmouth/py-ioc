#!/usr/bin/env python
"""
    Created by huangyi at 2020-09-02.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
import os

from test import ioc
from test.demo.presention.role_route import RoleRoute


if __name__ == '__main__':
    ioc.scan(__file__, "demo")
    role_name = RoleRoute().get_role_name(1)
    print(f"role name:{role_name}")
