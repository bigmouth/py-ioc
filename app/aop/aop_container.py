# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/2.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
import re
from abc import ABCMeta, abstractmethod

from app.aop.processor import Processor


class AopContainer(metaclass=ABCMeta):
    @abstractmethod
    def add_processor(self, processor_instance):
        pass

    @abstractmethod
    def get_processors(self, bean_name):
        pass


class NormalAopContainer(AopContainer):

    def __init__(self):
        self.processor_list = []

    def add_processor(self, processor_instance: Processor):
        self.processor_list.append(processor_instance)

    def get_processors(self, bean_name):
        result = []
        for processor in self.processor_list:
            pattern = processor.get_pattern()
            if re.match(pattern=pattern, string=bean_name):
                result.append(processor)
        return result
