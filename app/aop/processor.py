# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/2.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from abc import ABCMeta, abstractmethod, ABC


class ProcessorContext:

    def __init__(self):
        self.class_name = None
        self.method_name = None


class Processor(metaclass=ABCMeta):

    @abstractmethod
    def invoke(self, ctx: ProcessorContext, *args, **kwargs):
        pass

    @abstractmethod
    def get_pattern(self):
        pass

    @abstractmethod
    def set_pattern(self, pattern):
        pass

    @abstractmethod
    def get_next(self):
        pass

    @abstractmethod
    def set_next(self, processor):
        pass

    @abstractmethod
    def get_order(self):
        pass


class BaseProcessor(Processor, ABC):
    """
    处理器
    """
    def __init__(self, pattern):
        self.pattern = pattern
        self.next_processor = None

    def get_pattern(self):
        return self.pattern

    def set_pattern(self, pattern):
        self.pattern = pattern

    def get_next(self) -> Processor:
        return self.next_processor

    def set_next(self, processor):
        self.next_processor = processor

    def get_order(self):
        return 0


class BeanProcessor(BaseProcessor):

    def __init__(self, pattern, func):
        super(BeanProcessor, self).__init__(pattern)
        self.func = func

    def invoke(self, ctx: ProcessorContext, *args, **kwargs):
        return self.func(*args, **kwargs)
