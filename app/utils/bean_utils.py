# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/1.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""


class BeanUtils:

    @classmethod
    def get_full_class_name(cls, clazz):
        if isinstance(clazz, type(object)):
            return f"{clazz.__module__}.{clazz.__name__}"
        return None

    @classmethod
    def get_instance_full_class_name(cls, instance):
        return f"{instance.__class__.__module__}.{instance.__class__.__name__}"

    @classmethod
    def get_full_super_name(cls, clazz):
        if isinstance(clazz, type(object)):
            super_class_name = clazz.__base__.__name__
            if super_class_name == "object":
                return None
            return f"{clazz.__base__.__module__}.{clazz.__base__.__name__}"
        return None
