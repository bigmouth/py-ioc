# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/8/31.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from enum import Enum

from app.utils.bean_utils import BeanUtils


class BeanScope(Enum):
    Singleton = "singleton"
    Instance = "instance"


class BeanDefinition:

    def __init__(self, bean_class):
        self.bean_class = bean_class
        self.scope: BeanScope = BeanScope.Singleton

    def get_class_name(self):
        return BeanUtils.get_full_class_name(self.bean_class)

    def get_interface_name(self):
        return BeanUtils.get_full_super_name(self.bean_class)
