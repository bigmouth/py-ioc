# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/8/31.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from abc import ABCMeta, abstractmethod


class BeanFactory(metaclass=ABCMeta):

    @abstractmethod
    def get_bean(self, *args, **kwargs):
        """
        :param args:
        args[0]传str类型，则根据名称获取，传类，则根据类获取
        :param kwargs:
        :return:
        """
        pass

    @abstractmethod
    def get_bean_by_name(self, name):
        pass

    @abstractmethod
    def get_bean_by_type(self, type0):
        pass

    @abstractmethod
    def add_bean_by_interface(self, interface, bean_class, scope):
        pass

    @abstractmethod
    def add_bean_by_type(self, bean_class, scope):
        pass

    @abstractmethod
    def add_bean_by_name(self, name, bean_class, scope):
        pass
