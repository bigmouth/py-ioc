# -*- coding: utf-8 -*-
"""
    Created by huangyi at 2020/9/1.
    Copyright (c) 2013-present, Xiamen Dianchu Technology Co.,Ltd.
    Description:
    Changelog: all notable changes to this file will be documented
"""
from app.bean.bean_definition import BeanDefinition, BeanScope
from app.errors.unsupport import UnSupportException


class BeanInstantiateProxy:
    """
    bean实例化代理类
    """

    def __init__(self, bean_definition: BeanDefinition):
        self.bean_definition: BeanDefinition = bean_definition
        if bean_definition.scope == BeanScope.Singleton:
            self.bean_instance = self._create()

    def _create(self):
        return self.bean_definition.bean_class()

    def get_bean(self):
        if self.bean_definition.scope == BeanScope.Singleton:
            return self.bean_instance
        elif self.bean_definition.scope == BeanScope.Instance:
            return self.bean_definition.bean_class()
        else:
            raise UnSupportException("不支持的bean scope")
